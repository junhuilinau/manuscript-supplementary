#!/bin/bash

mkdir best

for seq in DIM.*
do

echo $seq

#merge the assembly
awk -F"\t" 'NR==FNR {a[$1]=$2; next} FNR>0 {$(NF+1)=a[$1]} 1' Accession_Assembly_Scaffolds.txt OFS="\t" $seq > $seq.new

# extract the best hit for each sequence
sort -k6,6nr -k5,5 -k3,3nr $seq.new | \
awk -F "\t" '
NR == 1 {key=$7}
$7 == key {print nr[NR] $0; key=$7}' > ./best/$seq.txt
rm $seq.new

done

cat ./best/*.txt > best.hit.txt

