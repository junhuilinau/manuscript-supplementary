#!/usr/bin/env bash
# This code is to calculate the average of all rarefactions generated using QIIME, written by Junhui Li

# usage: ./rarefaction_average.sh

chmod 755 rarefaction_average.sh
mkdir temp

# to add header of the first rarefaction
mv rarefaction_4000_0.biom.txt rarefaction_4000.biom.txt # this is based on rarefied results (4000 sequences)

for r0 in rarefaction_4000.biom.txt
do
sed -i '1d' $r0
sed 's/#OTU ID/OTUID/' $r0 > ./temp/rarefaction_4000_0.biom.txt
done


for r in rarefaction_4000_*.biom.txt
do
sed -i '2d' $r
sed 's/# Constructed from biom file//g' $r > ./temp/$r
done


# concentrate all the rarefied files
cat ./temp/rarefaction_4000_*.biom.txt > rarefaction.txt 

echo -e "#! /usr/bin/env Rscript
data <- read.table('rarefaction.txt', header=T, sep='\t', check.names=F)
head(data)
data1 <- aggregate(. ~ OTUID, data, mean)
write.table(data1,'rarfaction_ave.txt',quote=F,row.names = F,sep='\t')
quit()" > temp1.R

chmod 755 temp1.R
R < temp1.R --no-save

rm -r ./temp
rm temp1.R

mv rarfaction_ave.txt ..

rm *.txt
rm *.biom

