#!/bin/bash

# download the genome sequences, and built the loacal BLAST database, then BLAST the query sequences 
mkdir NCBI
cd ./NCBI

for g in $(cat genome.list.txt)

do
wget $g

done

cat *.gz | gunzip > ncbi.fna

mv ncbi.fna ..
rm *.gz

makeblastdb -in ncbi.fna -dbtype nucl -out $PWD/NCBI/ncbiCG -parse_seqids

blastn -query $PWD/dim*.fna -db $PWD/NCBI/ncbiCG -outfmt 6 -out $PWD/dimblast*.txt -num_threads 8

