Pipelines and scripts for the ISME paper of 'Predictive genomic traits for bacterial growth in culture versus actual growth in soil'.

We have problem in publishing the data in MG-RAST and tried to get technical support from MG-RAST several times. We did hear from MG-RAST team regarding our request but no follow-up. 
The data are available in the NCBI short-read archive under accession no. PRJNA521534. 